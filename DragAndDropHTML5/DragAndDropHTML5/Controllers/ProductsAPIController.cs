﻿using DragAndDropHTML5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DragAndDropHTML5.Controllers
{
    public class ProductsAPIController : ApiController
    {
        DbProductsEntities ctx;

        public ProductsAPIController()
        {
            ctx = new DbProductsEntities();
        }

        [Route("ListProducts")]
        public IEnumerable<Product> GetProducts()
        {
            return ctx.Products.ToList();
        }
    }
}
